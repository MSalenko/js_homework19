function countTimeBacklogIsDone(memberPoints, backlogPoints, deadline) {
  const endDate = new Date(deadline);
  const numDaysLeft = getBusinessDaysLeft(endDate);
  const teamPointsTotalPerDay = memberPoints.reduce(function (sum, current) {
    return sum + current;
  }, 0);

  const backlogPointsTotal = backlogPoints.reduce(function (sum, current) {
    return sum + current;
  }, 0);

  const numDaysOfWork = Math.ceil(backlogPointsTotal / teamPointsTotalPerDay);
  if (numDaysOfWork <= numDaysLeft) {
    alert(`All tasks will be done ${numDaysLeft - numDaysOfWork} days before Deadline`);
  } else {
    const hoursToEndBacklog = Math.ceil(
      ((backlogPointsTotal - numDaysLeft * teamPointsTotalPerDay) / teamPointsTotalPerDay) * 8
    );
    alert(`Developers need ${hoursToEndBacklog} hours more to end all tasks in the backlog`);
  }
}

function getBusinessDaysLeft(endDate) {
  let countDays = 0;
  const curDate = new Date();
  while (curDate <= endDate) {
    const dayOfWeek = curDate.getDay();
    if (dayOfWeek !== 0 && dayOfWeek !== 6) countDays++;
    curDate.setDate(curDate.getDate() + 1);
  }
  return countDays;
}
// Перевіряємо:
countTimeBacklogIsDone([3, 2, 5], [8, 5, 12], '02/06/2023');
countTimeBacklogIsDone([4, 6], [10, 5, 10], '02/24/2023');
